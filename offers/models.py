from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Offer(models.Model):
    
    destination = models.CharField(max_length=200)
    departure = models.CharField(max_length=200)
    date_from = models.DateField()
    date_to = models.DateField()
    description = models.TextField()
    price = models.DecimalField(max_digits=6, decimal_places=2)
    creation_date = models.DateField(auto_now_add=True)
    readonly_fields = ('creation_date')
    

    def __unicode__(self):
        return  self.departure + " - " + self.destination 


