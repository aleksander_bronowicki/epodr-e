from django.shortcuts import render, redirect
from django.views.generic import View, DetailView, ListView
from .models import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import JsonResponse
from django.core.mail import EmailMessage
from .forms import *

class OffersListView(ListView):
    model = Offer
    template_name = 'offers/offers_list.html'
    context_object_name = 'offers_list'
    def get_queryset(self):
        destination = self.request.GET.get('destination');
        date_from = self.request.GET.get('date_from');
        date_to = self.request.GET.get('date_to');
        offers = Offer.objects.order_by('creation_date')
        if destination:
            offers = offers.filter(destination=destination).order_by('creation_date')
        if destination:
            offers = offers.filter(date_from__gte=date_from).order_by('creation_date')
        if destination:
            offers = offers.filter(date_to__lte=date_to).order_by('creation_date')
      
        page = self.request.GET.get('page', 1)
        paginator = Paginator(offers, 8)
        try:
             offers = paginator.page(page)
        except PageNotAnInteger:
            offers = paginator.page(1)
        except EmptyPage:
            offers = paginator.page(paginator.num_pages)
        return offers;

    def get_context_data(self, **kwargs):
        context = super(OffersListView, self).get_context_data(**kwargs)
        search = self.request.GET.get('search');
        options = Offer.objects.values_list('destination',flat="true").distinct() 
        if search:
            context['search'] = self.request.GET.get('search')
        date_from = self.request.GET.get('date_from');
        date_to = self.request.GET.get('date_to');
        destination = self.request.GET.get('destination');
        context['options'] = options;
        if date_from:
            context['date_from'] = date_from;
        if date_to:
            context['date_to'] = date_to;
        if destination:
            context['destination'] = destination;
        return context

class OfferDetailView(DetailView):
    model = Offer
    template_name = 'offers/offer_details.html'
    context_object_name = 'offer'


class ContactView(View):
    form_class = ContactForm
    template_name = 'offers/success.html'
 

    def post(self, request):
        form = self.form_class(request.POST)

        if form.is_valid():
            email = form.cleaned_data['email']
            text = form.cleaned_data['text']
            offerId = form.cleaned_data['offerId']
            

            email = EmailMessage('Zapytanie o oferte: ' + str(offerId), text, to=['rolek93@gmail.com'],  headers = {'Replay-To': email})
            email.send()
            return render(request, self.template_name, {'offerId': offerId})