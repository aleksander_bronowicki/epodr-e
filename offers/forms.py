from django import forms


class ContactForm(forms.Form):
    offerId = forms.IntegerField(required=True)
    email = forms.EmailField(required=True)
    text = forms.CharField(required=True)
