from django.conf.urls import url
from .views import *
urlpatterns = [
    url(r'^$', OffersListView.as_view(), name='offers_list'),
    url(r'^(?P<pk>[0-9]+)/$', OfferDetailView.as_view(), name='offer_detail'),
    url(r'^sendmail/$', ContactView.as_view(), name='send_mail'),
   
] 