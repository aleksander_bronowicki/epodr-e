# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-23 22:36
from __future__ import unicode_literals

import ckeditor.fields
from django.db import migrations, models
import django.db.models.deletion
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0002_auto_20150616_2121'),
        ('relations', '0002_remove_triprelation_picture'),
    ]

    operations = [
        migrations.AddField(
            model_name='triprelation',
            name='tags',
            field=taggit.managers.TaggableManager(help_text='A comma-separated list of tags.', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Tags'),
        ),
        migrations.AlterField(
            model_name='relationimage',
            name='image',
            field=models.ImageField(upload_to='relations/%Y/%m/%d'),
        ),
        migrations.AlterField(
            model_name='relationimage',
            name='relation',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='relations.TripRelation'),
        ),
        migrations.AlterField(
            model_name='triprelation',
            name='relation_text',
            field=ckeditor.fields.RichTextField(),
        ),
    ]
