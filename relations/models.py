from __future__ import unicode_literals

from django.db import models
from django.forms import ModelForm
from taggit.managers import TaggableManager
from django.contrib.auth.models import User
from django.urls import reverse
from ckeditor.fields import RichTextField

class TripRelation(models.Model):
    
    title = models.CharField(max_length=200)
    text = RichTextField()
    creation_date = models.DateField(auto_now_add=True)
    tags = TaggableManager()
    user = models.ForeignKey(User)
    readonly_fields = ('creation_date')
    image = models.ImageField(upload_to='relations/%Y/%m/%d')
    

    def get_absolute_url(self):
        return reverse('relation_detail', kwargs={'pk': self.pk})

    def __unicode__(self):
        return self.title


