from django.conf.urls import url
from .views import *
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^$', RelationsListView.as_view(), name='relations_list'),
    url(r'^relations/$', RelationsListView.as_view(), name='relations_list'),
    url(r'^relations/(?P<pk>[0-9]+)/$', RelationDetailView.as_view(), name='relation_detail'),
    url(r'^relations/add/$',login_required(RelationCreateView.as_view()), name='relation_add'),

     
] 