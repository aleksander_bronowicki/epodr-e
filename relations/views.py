from django.shortcuts import render
from django.views.generic import View, DetailView, ListView
from django.views.generic.edit import CreateView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import *
# Create your views here.


class RelationsListView(ListView):
    model = TripRelation
    template_name = 'relations/relations_list.html'
    context_object_name = 'relations_list'
    def get_queryset(self):
        filter_val = self.request.GET.get('search');
        if filter_val:
            trips = TripRelation.objects.filter(tags__name__in=[filter_val]).order_by('creation_date')
        else:
            trips = TripRelation.objects.order_by('creation_date')

        page = self.request.GET.get('page', 1)
        paginator = Paginator(trips, 8)
        try:
             trips = paginator.page(page)
        except PageNotAnInteger:
            trips = paginator.page(1)
        except EmptyPage:
            trips = paginator.page(paginator.num_pages)
        return trips;

    def get_context_data(self, **kwargs):
        context = super(RelationsListView, self).get_context_data(**kwargs)
        search = self.request.GET.get('search');
   
        
        if search:
            context['search'] = search;     
        return context
            

class RelationDetailView(DetailView):
    model = TripRelation
    template_name = 'relations/relation_details.html'
    context_object_name = 'relation'

 
class RelationCreateView(CreateView ):
    model = TripRelation
    template_name = 'relations/relation_add.html'
    fields = ['title','text','image','tags']
    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(RelationCreateView, self).form_valid(form)

